require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  scenario "User accesses show page from root URL" do
    visit potepan_product_path(product.id)
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    within ".productsContent" do
      # @productと同じtaxonを持つ商品名とリンクが表示されている
      expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      # 関連商品の値段が表示されている
      expect(page).to have_content related_product.display_price
      # 関連商品に@productと同様の商品名が表示されていない
      expect(page).not_to have_content product.name
    end
  end
end
