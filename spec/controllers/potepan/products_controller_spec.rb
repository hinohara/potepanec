require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it "responds successfully" do
      expect(response).to be_successful
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end

    it "contains 4 related products" do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
